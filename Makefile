IMAGE ?= jigneshvasoya/docker-sgx
DOCKERFILE ?= Dockerfile

build: Dockerfile
	docker build --rm -t ${IMAGE}:building -f ${DOCKERFILE} .
	docker tag ${IMAGE}:building ${IMAGE}

rebuild: Dockerfile
	docker build --rm --no-cache -t ${IMAGE}:building -f ${DOCKERFILE} .
	docker tag ${IMAGE}:building ${IMAGE}

