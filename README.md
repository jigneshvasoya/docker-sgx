# SGX development environment

A Docker image with [Intel SGX](https://software.intel.com/en-us/sgx) [SDK and
PSW](https://github.com/01org/linux-sgx) (platform software, i.e., runtime).
You can use it as a base Docker image for your apps which use Intel SGX.

### For SGX machine

Intel SGX [kernel module](https://github.com/01org/linux-sgx-driver) has to be loaded on the
host and you have to provide it to the container when you run it:

```
docker run -it --device /dev/isgx --device /dev/mei0 --name test-sgx jigneshvasoya/docker-sgx
```

### For non-SGX machine
```
docker run -it --name test-sgx jigneshvasoya/docker-sgx
```

### Inside docker container

SDK is installed under `/opt/intel/sgxsdk`. You should do:

```
source /opt/intel/sgxsdk/environment
```

in your bash script to load the SDK environment.

### Quick play with SGX sample code

Build and run sample application provided with SDK

```
cd /opt/intel/sgxsdk/SampleCode/SampleEnclave/
make SGX_MODE=SIM SGX_DEBUG=1
./app
```
**Note**: replace sgx mode "SIM" to "HW" on SGX machines.
